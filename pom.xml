<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright (c) 2016, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
  ~
  ~ WSO2 Inc. licenses this file to you under the Apache License,
  ~ Version 2.0 (the "License"); you may not use this file except
  ~ in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~ http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied. See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <groupId>io.entgra.product.community</groupId>
    <artifactId>product-parent</artifactId>
    <packaging>pom</packaging>
    <version>5.0.1-SNAPSHOT</version>
    <name>Entgra Community Product - Parent</name>
    <url>http://entgra.io</url>
    <description>Entgra Community Product</description>

    <parent>
        <groupId>org.wso2</groupId>
        <artifactId>wso2</artifactId>
        <version>2</version>
    </parent>

    <modules>
        <module>iot-core</module>
        <module>p2-profile</module>
        <module>distribution</module>
    </modules>

    <dependencyManagement>
        <dependencies>
            <!--CDM core dependencies-->
            <dependency>
                <groupId>org.wso2.carbon.devicemgt</groupId>
                <artifactId>org.wso2.carbon.device.mgt.core</artifactId>
                <version>${carbon.device.mgt.version}</version>
            </dependency>
            <dependency>
                <groupId>org.wso2.carbon.devicemgt</groupId>
                <artifactId>org.wso2.carbon.device.mgt.common</artifactId>
                <version>${carbon.device.mgt.version}</version>
            </dependency>
            <dependency>
                <groupId>org.wso2.carbon.devicemgt</groupId>
                <artifactId>org.wso2.carbon.device.mgt.group.core</artifactId>
                <version>${carbon.device.mgt.version}</version>
            </dependency>
            <dependency>
                <groupId>org.wso2.carbon.devicemgt</groupId>
                <artifactId>org.wso2.carbon.device.mgt.group.common</artifactId>
                <version>${carbon.device.mgt.version}</version>
            </dependency>
            <dependency>
                <groupId>org.wso2.carbon.devicemgt</groupId>
                <artifactId>org.wso2.carbon.policy.mgt.common</artifactId>
                <version>${carbon.device.mgt.version}</version>
            </dependency>
            <dependency>
                <groupId>org.wso2.carbon.devicemgt</groupId>
                <artifactId>org.wso2.carbon.policy.mgt.core</artifactId>
                <version>${carbon.device.mgt.version}</version>
            </dependency>
            <dependency>
                <groupId>org.wso2.carbon.devicemgt</groupId>
                <artifactId>org.wso2.carbon.certificate.mgt.core</artifactId>
                <version>${carbon.device.mgt.version}</version>
            </dependency>
            <dependency>
                <groupId>org.wso2.carbon.devicemgt</groupId>
                <artifactId>org.wso2.carbon.identity.jwt.client.extension</artifactId>
                <version>${carbon.device.mgt.version}</version>
            </dependency>
            <dependency>
                <groupId>org.wso2.carbon.devicemgt</groupId>
                <artifactId>org.wso2.carbon.device.mgt.oauth.extensions</artifactId>
                <version>${carbon.device.mgt.version}</version>
            </dependency>

            <!--Orbit dependencies-->
            <dependency>
                <groupId>org.wso2.orbit.com.h2database</groupId>
                <artifactId>h2</artifactId>
                <version>${orbit.h2.engine.version}</version>
            </dependency>

            <dependency>
                <groupId>io.entgra.product.community</groupId>
                <artifactId>entgra-iot-core</artifactId>
                <version>${project.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <properties>

        <wso2am-nexus-artifact-version>4.0.0</wso2am-nexus-artifact-version>
        <wso2am>wso2am-4.0.0</wso2am>

        <entgra-iot-core>entgra-iot-core-${project.version}</entgra-iot-core>
        <entgra-iot-community>entgra-iot-community-${project.version}</entgra-iot-community>
        <entgra-emm-community>entgra-emm-community-${project.version}</entgra-emm-community>

        <!-- Carbon Device Management-->
        <carbon.device.mgt.version>5.0.6-SNAPSHOT</carbon.device.mgt.version>

        <carbon.p2.plugin.version>5.1.2</carbon.p2.plugin.version>

        <!--Orbit versions-->
        <orbit.h2.engine.version>1.4.199.wso2v1</orbit.h2.engine.version>

        <carbon.device.mgt.plugin.version>6.0.2-SNAPSHOT</carbon.device.mgt.plugin.version>


    </properties>

    <scm>
        <url>https://gitlab.com/entgra/community-product.git</url>
        <developerConnection>scm:git:https://gitlab.com/entgra/community-product.git</developerConnection>
        <connection>scm:git:https://gitlab.com/entgra/community-product.git</connection>
        <tag>HEAD</tag>
    </scm>

    <build>
        <extensions>
            <extension>
                <groupId>org.apache.maven.wagon</groupId>
                <artifactId>wagon-ssh</artifactId>
                <version>2.1</version>
            </extension>
        </extensions>

        <plugins>
            <plugin>
                <groupId>org.jvnet.maven.incrementalbuild</groupId>
                <artifactId>incremental-build-plugin</artifactId>
                <version>1.3</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>incremental-build</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>2.3.1</version>
                <configuration>
                    <encoding>UTF-8</encoding>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <configuration>
                    <preparationGoals>clean install</preparationGoals>
                    <autoVersionSubmodules>true</autoVersionSubmodules>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
            </plugin>
        </plugins>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.felix</groupId>
                    <artifactId>maven-scr-plugin</artifactId>
                    <version>1.7.2</version>
                    <executions>
                        <execution>
                            <id>generate-scr-scrdescriptor</id>
                            <goals>
                                <goal>scr</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.felix</groupId>
                    <artifactId>maven-bundle-plugin</artifactId>
                    <version>2.3.5</version>
                    <extensions>true</extensions>
                    <configuration>
                        <obrRepository>NONE</obrRepository>
                        <!--<instructions>
                          <_include>-osgi.bnd</_include>
                        </instructions>-->
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-source-plugin</artifactId>
                    <version>2.1.2</version>
                    <executions>
                        <execution>
                            <id>attach-sources</id>
                            <phase>verify</phase>
                            <goals>
                                <goal>jar-no-fork</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-assembly-plugin</artifactId>
                    <version>3.0.0</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-project-info-reports-plugin</artifactId>
                    <version>2.4</version>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>build-helper-maven-plugin</artifactId>
                    <version>1.8</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>2.18.1</version>
                    <!--Need to remove below configuration after fixing tests-->
                    <configuration>
                        <testFailureIgnore>true</testFailureIgnore>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <pluginRepositories>
        <pluginRepository>
            <id>wso2.releases</id>
            <name>WSO2 internal Repository</name>
            <url>http://maven.wso2.org/nexus/content/repositories/releases/</url>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
                <checksumPolicy>ignore</checksumPolicy>
            </releases>
        </pluginRepository>
        <pluginRepository>
            <id>wso2.snapshots</id>
            <name>Apache Snapshot Repository</name>
            <url>http://maven.wso2.org/nexus/content/repositories/snapshots/</url>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
            </snapshots>
            <releases>
                <enabled>false</enabled>
            </releases>
        </pluginRepository>
        <pluginRepository>
            <id>wso2-nexus</id>
            <name>WSO2 internal Repository</name>
            <url>http://maven.wso2.org/nexus/content/groups/wso2-public/</url>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
                <checksumPolicy>ignore</checksumPolicy>
            </releases>
        </pluginRepository>
    </pluginRepositories>


    <repositories>
        <!-- Before adding ANYTHING in here, please start a discussion on the dev list.
             Ideally the Axis2 build should only use Maven central (which is available
             by default) and nothing else. We had troubles with other repositories in
             the past. Therefore configuring additional repositories here should be
             considered very carefully. -->
        <repository>
            <id>wso2-nexus</id>
            <name>WSO2 internal Repository</name>
            <url>http://maven.wso2.org/nexus/content/groups/wso2-public/</url>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
                <checksumPolicy>ignore</checksumPolicy>
            </releases>
        </repository>
        <repository>
            <id>wso2.releases</id>
            <name>WSO2 internal Repository</name>
            <url>http://maven.wso2.org/nexus/content/repositories/releases/</url>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
                <checksumPolicy>ignore</checksumPolicy>
            </releases>
        </repository>
        <repository>
            <id>wso2.snapshots</id>
            <name>WSO2 Snapshot Repository</name>
            <url>http://maven.wso2.org/nexus/content/repositories/snapshots/</url>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
            </snapshots>
            <releases>
                <enabled>false</enabled>
            </releases>
        </repository>
        <repository>
            <id>entgra-nexus</id>
            <name>Entgra internal Repository</name>
            <url>http://nexus.entgra.io/repository/maven-public/</url>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
                <checksumPolicy>ignore</checksumPolicy>
            </releases>
        </repository>
        <repository>
            <id>entgra.snapshots</id>
            <name>Entgra Snapshot Repository</name>
            <url>http://nexus.entgra.io/repository/maven-snapshots/</url>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
            </snapshots>
            <releases>
                <enabled>false</enabled>
            </releases>
        </repository>
        <repository>
            <id>entgra.releases</id>
            <name>Entgra internal Repository</name>
            <url>http://nexus.entgra.io/repository/maven-releases/</url>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
                <checksumPolicy>ignore</checksumPolicy>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
    </repositories>
    <distributionManagement>
        <snapshotRepository>
            <id>maven-snapshots</id>
            <url>http://nexus.entgra.io/repository/maven-snapshots/</url>
        </snapshotRepository>
        <repository>
            <id>maven-release</id>
            <url>http://nexus.entgra.io/repository/maven-releases/</url>
        </repository>
    </distributionManagement>
</project>
